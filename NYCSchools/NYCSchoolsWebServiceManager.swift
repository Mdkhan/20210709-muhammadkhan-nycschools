//
//  NYCSchoolsWebServiceManager.swift
//  NYCSchools
//
//  Created by Muhammad Khan on 7/8/21.
//

import Foundation

//Enum for API URLs.
enum NYCSchoolServiceType: String {
    case schoolListURL = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
    case satDataURL    = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
}

struct NYCSchoolsWebServiceManager {
    static func getSchoolsList( withCompletion completionHandler: @escaping (_ data: [NYCSchoolsListModal]?, _ error: Error?) -> Void) {
        //Create the URL
        guard let url = URL(string: NYCSchoolServiceType.schoolListURL.rawValue) else {return}
        #if DEBUG
        print("Base Url: \(url)")
        #endif
        //create the session object
        let session = URLSession.shared
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            guard error == nil else {
                #if DEBUG
                print("Response: Error occured: \(String(describing:error))")
                #endif
                completionHandler(nil, error)
                return
            }
            guard let data = data else {
                #if DEBUG
                print("Response: response object is empty")
                #endif
                return
            }
            do {
                //Parse json object.
                if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [Any] {
                    #if DEBUG
                    print("Response: \(json)")
                    #endif
                    let wrapper = try? JSONDecoder().decode([NYCSchoolsListModal].self, from: data)
                    completionHandler(wrapper, nil)
                }
            } catch let error {
                #if DEBUG
                print("Error:\(error.localizedDescription)")
                #endif
            }
        })
        task.resume()
    }
    
    static func getSATScoreData(_ queryItem: String, withCompletion completionHandler: @escaping (_ data: [NYCSchoolSATDataModal]?, _ error: Error?) -> Void) {
        //Create the URL
        guard let url = URL(string: NYCSchoolServiceType.satDataURL.rawValue + "?dbn=" + queryItem) else {return}
        #if DEBUG
        print("Base Url: \(url)")
        #endif
        //create the session object
        let session = URLSession.shared
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            guard error == nil else {
                #if DEBUG
                print("Response: Error occured: \(String(describing:error))")
                #endif
                completionHandler(nil, error)
                return
            }
            guard let data = data else {
                #if DEBUG
                print("Response: response object is empty")
                #endif
                return
            }
            do {
                //Parse json object.
                if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [Any] {
                    #if DEBUG
                    print("Response: \(json)")
                    #endif
                    let wrapper = try? JSONDecoder().decode([NYCSchoolSATDataModal].self, from: data)
                    completionHandler(wrapper, nil)
                }
            } catch let error {
                #if DEBUG
                print("Error:\(error.localizedDescription)")
                #endif
            }
        })
        task.resume()
    }
}
