//
//  NYCSchoolsListModal.swift
//  NYCSchools
//
//  Created by Muhammad Khan on 7/8/21.
//

import Foundation
struct NYCSchoolsListModal: Decodable {
    let school_name: String?
    let dbn: String?
}

struct NYCSchoolSATDataModal: Decodable {
    let school_name: String
    let num_of_sat_test_takers: String
    let sat_critical_reading_avg_score: String
    let sat_math_avg_score: String
    let sat_writing_avg_score: String
}
