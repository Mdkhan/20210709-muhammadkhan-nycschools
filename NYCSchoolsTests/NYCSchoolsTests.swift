//
//  NYCSchoolsTests.swift
//  NYCSchoolsTests
//
//  Created by Muhammad Khan on 7/8/21.
//

import XCTest
@testable import NYCSchools

class NYCSchoolsTests: XCTestCase {

    var jsonObject:[[String: String]] = [[:]]
    var dataDic:[NYCSchoolsListModal] = []
  //  var firstObject:ISSTrackMappable?
    var storyBoard:UIStoryboard?
    var vc: NYCSchoolsTableViewController?
    
    override func setUp() {
        super.setUp()
        
        //TO DO: Good to move this to a .json file and pull to test without adding in class.
        jsonObject = [[ "dbn" : "02M260", "school_name" : "Clinton School Writers & Artists, M.S. 260"], [ "dbn" : "02M260","school_name" : "Clinton School Writers & Artists, M.S. 260"]]
        
        if let bundle = Bundle.main.bundleIdentifier{
            storyBoard = UIStoryboard(name: "Main", bundle: Bundle(identifier:bundle))
        }
        guard let vwController = storyBoard?.instantiateViewController(withIdentifier: "NYCSchoolsTableVC") as? NYCSchoolsTableViewController else{
            XCTAssert(false, "NYCSchoolsTableViewController doesn't have identifier NYCSchoolsTableVC")
            return
        }
        vc = vwController
        vc?.loadView()
        vc?.viewDidLoad()
    }
    
    //MARK: Test View delagate and datasource test
    func testTableViewDelegateMethods() {
        guard let vcUnwrapped = vc else {
            XCTAssert(false, "View Controller is nil")
            return
        }
        let arrayOfObject = try? JSONSerialization.data(withJSONObject: jsonObject, options: [])
        guard let jsonData = try? JSONDecoder().decode([NYCSchoolsListModal].self, from: arrayOfObject ?? Data()) else {
           return XCTAssert(false, "List is nil")
        }
        dataDic = jsonData
        vcUnwrapped.schoolsList = dataDic

        let indexpath = IndexPath(row: 0, section: 0)
        _ = vc?.tableView(vc?.tableView ?? UITableView(), cellForRowAt: indexpath)
        _ = vc?.tableView(vc?.tableView ?? UITableView(), didSelectRowAt: indexpath)
        XCTAssertEqual(vcUnwrapped.tableView.numberOfRows(inSection: 0), dataDic.count,"Table view number of section is more or less than 1.")
        XCTAssertEqual(vcUnwrapped.tableView.separatorStyle, .singleLine,"Table view cell sepator style not set to singleLine")
    }
}
