//
//  NYCSchoolSATDataViewController.swift
//  NYCSchools
//
//  Created by Muhammad Khan on 7/9/21.
//

import UIKit

class NYCSchoolSATDataViewController: UIViewController {
    
    struct NYCSchoolSATDataViewConstant {
        static let pageTitle            = "SAT Data"
        static let emptyViewHeaderTxt   = "Oops"
        static let emptyViewBodyTxt     = "We dont have enough data for this school."
        static let testTakersLblTxt     = "Number of SAT test takers:"
        static let readingAvgLblTxt     = "Reading Average Score"
        static let mathAvgLblTxt        = "Math Average Score"
        static let writingAvgLblTxt     = "Writing Average Score"
    }
    
    //View with Data
    @IBOutlet weak var dataView: UIView!
    @IBOutlet weak var dataViewHeader: UILabel!
    @IBOutlet weak var testTakersLbl: UILabel!
    @IBOutlet weak var testTakersValueLbl: UILabel!
    @IBOutlet weak var readingAvgLbl: UILabel!
    @IBOutlet weak var readingAvgValueLbl: UILabel!
    @IBOutlet weak var mathAvgLbl: UILabel!
    @IBOutlet weak var mathAvgValueLbl: UILabel!
    @IBOutlet weak var writingAvgLbl: UILabel!
    @IBOutlet weak var writingAvgValueLbl: UILabel!
    
    //View without Data
    @IBOutlet weak var emptyDataView: UIView!
    @IBOutlet weak var emptyViewHeader: UILabel!
    @IBOutlet weak var emptyViewBody: UILabel!
    
    var schoolSATData: [NYCSchoolSATDataModal] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Nav Bar Title
        navigationItem.title = NYCSchoolSATDataViewConstant.pageTitle
        // Setting up UI based on SAT data we will get
        findStatusOfData()
    }
    
    private func findStatusOfData() {
        if schoolSATData.isEmpty {
            //Setting UI for No data View
            dataView.isHidden = true
            emptyDataView.isHidden = false
            emptyViewHeader.text = NYCSchoolSATDataViewConstant.emptyViewHeaderTxt
            emptyViewBody.text = NYCSchoolSATDataViewConstant.emptyViewBodyTxt
        } else {
            //Setting UI when we have data
            dataView.isHidden = false
            emptyDataView.isHidden = true
            dataViewHeader.text = schoolSATData.first?.school_name
            testTakersLbl.text = NYCSchoolSATDataViewConstant.testTakersLblTxt
            testTakersValueLbl.text = schoolSATData.first?.num_of_sat_test_takers
            readingAvgLbl.text = NYCSchoolSATDataViewConstant.readingAvgLblTxt
            readingAvgValueLbl.text = schoolSATData.first?.sat_critical_reading_avg_score
            mathAvgLbl.text = NYCSchoolSATDataViewConstant.mathAvgLblTxt
            mathAvgValueLbl.text = schoolSATData.first?.sat_math_avg_score
            writingAvgLbl.text = NYCSchoolSATDataViewConstant.writingAvgLblTxt
            writingAvgValueLbl.text = schoolSATData.first?.sat_writing_avg_score
        }
    }
}
