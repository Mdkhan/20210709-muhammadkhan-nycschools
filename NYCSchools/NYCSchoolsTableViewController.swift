//
//  NYCSchoolsTableViewController.swift
//  NYCSchools
//
//  Created by Muhammad Khan on 7/8/21.
//

import UIKit

class NYCSchoolsTableViewController: UITableViewController {
    
    var schoolsList: [NYCSchoolsListModal?] = []
    var topSpace:CGFloat = 0.0
    private let reuseIdentifier = "SchoolsListCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getSchoolData()
        //Nav Bar Title
        navigationItem.title = "Schools List"
    }
    
    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return schoolsList.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier) ?? createSchoolNamesCell()
        cell.textLabel?.text = schoolsList[indexPath.row]?.school_name
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard schoolsList[indexPath.row]?.dbn == nil else {
            return getSATData(queryValue: schoolsList[indexPath.row]?.dbn ?? "")
        }
    }
    
    //MARK: UITableViewCell To Show On TableView.
    private func createSchoolNamesCell() -> UITableViewCell {
        let newCell = UITableViewCell(style: .subtitle, reuseIdentifier: reuseIdentifier)
        newCell.textLabel?.font = UIFont.systemFont(ofSize: 15.0)
        newCell.textLabel?.textColor = .gray
        newCell.textLabel?.numberOfLines = 0
        newCell.accessoryType = .disclosureIndicator
        newCell.separatorInset = UIEdgeInsets(top: 0,left: 8,bottom: 0,right: 8)
        return newCell
    }
    
    @IBAction func exitBarButtonTapped(_ sender: Any) {
        exit(0)
    }
    
    //API Call to get school list Data
    func getSchoolData() {
        NYCSchoolsWebServiceManager.getSchoolsList(withCompletion: {(data: [NYCSchoolsListModal]? , error: Error?) -> Void in
            if error != nil {
                DispatchQueue.main.async {
                    // creating the alert for error scenario
                    let alert = UIAlertController(title: "Error", message: "Please try again later", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            } else {
                if let data = data {
                    DispatchQueue.main.async {
                        self.schoolsList = data
                        self.tableView.reloadData()
                    }
                }
            }
        })
    }
    
    //API Call to get SAT Data based on user selection
    func getSATData(queryValue: String) {
        NYCSchoolsWebServiceManager.getSATScoreData(queryValue, withCompletion: {(data: [NYCSchoolSATDataModal]? , error: Error?) -> Void in
            if error != nil {
                DispatchQueue.main.async {
                    // creating the alert for error scenario
                    let alert = UIAlertController(title: "Error", message: "Please try again later", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            } else {
                if let data = data {
                    DispatchQueue.main.async {
                        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "NYCSchoolSATDataVC") as? NYCSchoolSATDataViewController {
                            vc.schoolSATData = data
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                    }
                }
            }
        })
    }
}
